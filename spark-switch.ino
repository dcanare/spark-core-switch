#include "switch.h"

Switch lightSwitch;

void setup(){
    // buttons on 2, 3
    // NPN transistor (to servo power) on 8
    // servo on A7
    lightSwitch.setup(2, 3, 8, A7);
    
	Spark.function("flip", flip);
	Spark.function("enable", enable);
	Spark.function("disable", disable);
}

void loop(){
    lightSwitch.update();
}



int flip(String args){
	lightSwitch.toggle();
	return 0;
}

int enable(String args){
	lightSwitch.turnOn();
	return 0;
}

int disable(String args){
	lightSwitch.turnOff();
	return 0;
}
