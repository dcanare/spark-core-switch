#include "application.h"
#include "switch.h"

Servo switchServo;

void Switch::setup(int upSwitchPin, int downSwitchPin, int servoEnablePin, int servoControlPin){
    upSwitch = upSwitchPin;
    downSwitch = downSwitchPin;
    servoEnable = servoEnablePin;
    servoControl = servoControlPin;
    
	pinMode(upSwitch, INPUT_PULLUP);
	pinMode(downSwitch, INPUT_PULLUP);
	pinMode(servoEnable, OUTPUT);
	turnOn();
}

void Switch::update(){
	if(isUpActivated()){
		turnOn();
	}else if(isDownActivated()){
		turnOff();
	}
}

bool Switch::isUpActivated(){
    return digitalRead(upSwitch) == LOW;
}

bool Switch::isDownActivated(){
    return digitalRead(downSwitch) == LOW;
}

void Switch::doSwitch(int angle){
	digitalWrite(servoEnable, HIGH);

	switchServo.attach(servoControl);
	switchServo.write(angle);
	delay(500);

	digitalWrite(servoEnable, LOW);
	switchServo.detach();
}

void Switch::turnOn(){
	doSwitch(onAngle);
	shouldBeOn = true;
}

void Switch::turnOff(){
	doSwitch(offAngle);
	shouldBeOn = false;
}

void Switch::toggle(){
	if(shouldBeOn){
		turnOff();
	}else{
		turnOn();
	}
}

void Switch::setOnAngle(int angle){
    onAngle = angle;
}

void Switch::setOffAngle(int angle){
    offAngle = angle;
}
