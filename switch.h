/**
 * An internet-enabled, retrofit light switch controller
 * 
 * A standard light switch (toggle) is controlled via servo.
 * 2 buttons can be used for manual control (on/off), but
 * The servo is disabled between calls, so you should be able
 * to physically toggle the switch.
 * 
 * @author Dominic Canare <dom@makeict.org>
 * 
 **/

class Switch{

    public:
        // Set pin numbers
        void setup(int upDetectPin, int downDetectPin, int servoEnablePin, int servoControlPin);
        
        // Checks for button states to flip the switch
        void update();
        
        void turnOn();
        void turnOff();
        void toggle();
        
        bool isUpActivated();
        bool isDownActivated();
    
        void setOnAngle(int angle);
        void setOffAngle(int angle);
        
    private:
        int upSwitch, downSwitch, servoEnable, servoControl;
        int onAngle = 120;
        int offAngle = 60;
        bool shouldBeOn = false;
        
        void doSwitch(int angle);
};
